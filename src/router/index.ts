import
{
  createWebHashHistory,
  createRouter,
  type RouteRecordRaw,
} from "vue-router";


const routes: Array<RouteRecordRaw> = [
  {
    path: "/settings",
    name: "Settings",
    component: () => import( "@/views/Settings.vue" ),
  },
];
const router = createRouter( {
  history: createWebHashHistory(),
  routes,
} );

export default router;

import "./styles.css";
import App from "./App.vue";
import "virtual:uno.css";
import router from "./router";
import pinia from "./store";
import { createApp } from "vue";



const app = createApp( App );

app.use( router ).use( pinia ).mount( "#app" );

import { useDark } from "@vueuse/core";
import { darkTheme, enUS, lightTheme, zhCN } from "naive-ui";
import { computed } from "vue";
import { useRoute } from "vue-router";
import DefaultLayout from "@/layouts/DefaultLayout.vue";
import TrayLayout from "@/layouts/TrayLayout.vue";

export const isDark = useDark();
export const useTheme = computed(() => {
  return isDark.value ? darkTheme : lightTheme;
});
export const useLayout = computed(() => {
  return useRoute().meta.layout == "TrayLayout" ? TrayLayout : DefaultLayout;
});

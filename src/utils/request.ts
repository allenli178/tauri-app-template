import { fetch, Body, ResponseType } from '@tauri-apps/api/http'
import config from '@/config'
import { BaseDirectory, appCacheDir, appDataDir } from '@tauri-apps/api/path';
import { writeBinaryFile } from '@tauri-apps/api/fs';
import { nanoid } from 'nanoid'
export const fetchImgs = async ( data: Object ) =>
{
    const response = await fetch( config.wukong_api.endpoint, {
        method: 'POST',
        body: Body.json( data ),
        headers: {
            'Content-Type': 'application/json',
            'token': config.wukong_api.token
        }, timeout: 30,
    } )
    return response.data
}

export async function downloadFile ( url: string )
{
    const message = useMessage()
    const response = await fetch( url, {
        method: 'GET',
        responseType: ResponseType.Binary
    } )
    const basePath = nanoid() + '.png';
    console.log( "basePath: ", basePath )
    console.log( "appCacheDir: ", BaseDirectory.AppData )
    console.log( "appDataDir: ", response)
    writeBinaryFile(
        basePath, response.data as any,
        { dir: BaseDirectory.AppData } ).then( res =>
        {
            message.info( '下载成功' )
        } )
}